<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>

<c:set var='view' value='cards' scope='session'/>

<div id="centerColumn">

    <h3>Карты
    </h3>
    <table width="100%" border="0" cellspacing="0" cellpadding="10">
        <tr>
            <td width="33%">Номер карты</td>
            <td width="33%">Пин-код</td>
            <td width="33%">Конечная дата</td>
        </tr>
        <c:forEach var="card" items="${account.cards}">

            <tr>
                <td>${card.id}</td>
                <td>${card.pinCode}</td>
                <td>${card.expiresDay.getTime()} </td>
            </tr>
        </c:forEach>

    </table>

</div>