package dao.daoImplementation;

import dao.DAOFactory;
import dao.daoInterfaces.AdministratorDAO;
import model.Administrator;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

import static dao.DAOJDBCUtil.close;

public class AdministratorDAOJDBC implements AdministratorDAO {

    private static final String SQL_FIND_ADMIN = "SELECT * FROM administrator WHERE login=(?) AND password=(?)";

    private DAOFactory daoFactory;

    public AdministratorDAOJDBC(DAOFactory daoFactory) {
        this.daoFactory = daoFactory;
    }

    @Override
    public Administrator getAdmin(String login, String password) {
        Connection connection = null;
        PreparedStatement preparedStatement = null;
        ResultSet resultSet = null;
        Administrator administrator = null;

        try {
            connection = daoFactory.getConnection();
            preparedStatement = connection.prepareStatement(SQL_FIND_ADMIN);
            preparedStatement.setString(1, login);
            preparedStatement.setString(2, password);
            resultSet = preparedStatement.executeQuery();
            while (resultSet.next()) {
                administrator = map(resultSet);
            }

        } catch (SQLException e) {
            e.printStackTrace();
        } finally {
            close(resultSet);
            close(preparedStatement);
            close(connection);
        }
        return administrator;
    }

    private Administrator map(ResultSet resultSet) throws SQLException {
        Administrator administrator = new Administrator();
        administrator.setLogin(resultSet.getString(1));
        administrator.setPassword(resultSet.getString(2));
        return administrator;
    }
}
