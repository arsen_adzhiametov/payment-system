package action;

import dao.DAOFactory;
import dao.DataSourceDAOFactory;
import dao.daoInterfaces.AccountDAO;
import model.Account;
import org.apache.log4j.Logger;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.math.BigDecimal;
import java.math.RoundingMode;

public class UpdateAccountAction implements Action {

    private static final Logger LOG = Logger.getLogger(UpdateAccountAction.class);

    @Override
    public String execute(HttpServletRequest request, HttpServletResponse response) throws Exception {
        DAOFactory daoFactory = DataSourceDAOFactory.getDAOFactory();
        AccountDAO accountDAO = daoFactory.getAccountDAO();
        Account account = (Account) request.getSession().getAttribute("account");

        String amount = request.getParameter("amount");
        String pattern = "^(?=.+)(?:[1-9]\\d*|0)?(?:\\.\\d+)?$";
        if (!amount.matches(pattern)) {
            LOG.warn("Invalid amount inserted in form!");
            request.setAttribute("error", "Неверная сумма. Попробуйте еще раз");
            return "account";
        }

        Double total;

        if (account.isFrozen()) {
            LOG.warn("Frozen account tried to change something");
            request.setAttribute("error", "Внимание! Ваш счет был заморожен по какой-то причине. Его может разблокировать только один адимнистратор во вселенной.");
            return "account";
        }

        if (request.getPathInfo().equals("/recharge")) {
            total = accountDAO.findAccount(account.getId()).getMoney() + Double.parseDouble(amount);
            LOG.info("Try to add money to account " + account.getId());
        } else {
            total = accountDAO.findAccount(account.getId()).getMoney() - Double.parseDouble(amount);
            LOG.info("Try to pay money from account " + account.getId());
        }

        if (total < 0) {
            request.setAttribute("error", "Вы не можете оплатить такой счет.");
            return "account";
        }

        BigDecimal formattedTotal = new BigDecimal(total);
        formattedTotal = formattedTotal.setScale(2, RoundingMode.HALF_UP);
        total = formattedTotal.doubleValue();

        boolean updated = accountDAO.updateAccount(account.getId(), total);

        if (updated) {
            account.setMoney(total);
            request.getSession().setAttribute("account", account);
            LOG.info("Account money updated");
            new PaymentAction().execute(request, response);
            return "account";
        } else {
            LOG.error("Account money not updated");
            request.setAttribute("error", "Не удалось пополнить ваш счет. Простите.");
            return "failed";
        }
    }
}
