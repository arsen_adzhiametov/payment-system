<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>

<c:set var='view' value='client_profile' scope='session'/>

<div id="indexLeftColumn">

    <h3><fmt:message key="profile"/></h3> <br> <br>

    <div id="profile_layout">
        ${sessionScope.client.name}
        ${sessionScope.client.lastName} <br>
        ${sessionScope.client.email}
    </div>
</div>

<div id="indexRightColumn">

    <h3><fmt:message key="accounts"/><br> <br>

        <div id="account_layout" style="overflow-y: scroll; height: 250px">

            <table>
                <c:forEach var="account" items="${client.accounts}">
                    <tr>
                        <td>
                            <form method="post" action="/paymentsystem/select">
                                <input type="hidden" name="accountId" value="${account.id}"/>
                                <button><fmt:message key="accountNumber"/> ${account.id}</button>
                            </form>
                        </td>
                    </tr>
                </c:forEach>

            </table>

        </div>
        <br>
        <tr>
            <td>
                <form method="post" action="/paymentsystem/addAccount">
                    <%--<input type="hidden" name="accountId" value="${account.id}" />--%>
                    <button class="button"><fmt:message key="addAccount"/></button>
                </form>
            </td>
            <td>&nbsp;</td>
        </tr>

    </h3>

</div>
