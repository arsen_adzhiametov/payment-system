package dao.daoImplementation;

import dao.DAOFactory;
import dao.daoInterfaces.AccountDAO;
import dao.daoInterfaces.CardDAO;
import dao.daoInterfaces.PaymentDAO;
import model.Account;

import java.sql.*;
import java.util.ArrayList;
import java.util.List;

import static dao.DAOJDBCUtil.close;

public class AccountDAOJDBC implements AccountDAO {

    private static final String SQL_CREATE_ACCOUNT = "INSERT INTO account (money, client_id) VALUES (?, ?)";
    private static final String SQL_FIND_ACCOUNT_ID_BY_CLIENT_ID = "SELECT id FROM account WHERE client_id=(?)";
    private static final String SQL_FREEZE_ACCOUNT = "UPDATE account SET is_frozen = 1 WHERE id=(?)";
    private static final String SQL_UNFREEZE_ACCOUNT = "UPDATE account SET is_frozen = 0 WHERE id=(?)";
    private static final String SQL_FIND_ACCOUNTS_BY_CLIENT_ID = "SELECT * FROM account WHERE client_id=(?)";
    private static final String SQL_FIND_ACCOUNT_BY_ID = "SELECT * FROM account WHERE id=(?)";
    private static final String SQL_UPDATE_ACCOUNT = "UPDATE account SET money = (?) WHERE id=(?)";
    private static final String SQL_FIND_ALL_FROZEN = "SELECT * FROM account WHERE is_frozen=TRUE";

    private DAOFactory daoFactory;

    public AccountDAOJDBC(DAOFactory daoFactory) {
        this.daoFactory = daoFactory;
    }

    @Override
    public Account findAccount(Integer id) {
        Connection connection = null;
        PreparedStatement preparedStatement = null;
        ResultSet resultSet = null;
        Account account = null;
        try {
            connection = daoFactory.getConnection();
            preparedStatement = connection.prepareStatement(SQL_FIND_ACCOUNT_BY_ID);
            preparedStatement.setInt(1, id);
            resultSet = preparedStatement.executeQuery();
            while (resultSet.next()) {
                account = map(resultSet);
            }
        } catch (SQLException ex) {
            ex.printStackTrace();
        } finally {
            close(preparedStatement);
            close(connection);
        }
        return account;
    }

    @Override
    public List<Account> findAll(Integer clientId) {
        Connection connection = null;
        PreparedStatement preparedStatement = null;
        ResultSet resultSet = null;
        List<Account> accounts = new ArrayList<>();
        try {
            connection = daoFactory.getConnection();
            preparedStatement = connection.prepareStatement(SQL_FIND_ACCOUNTS_BY_CLIENT_ID);
            preparedStatement.setInt(1, clientId);
            resultSet = preparedStatement.executeQuery();
            while (resultSet.next()) {
                accounts.add(map(resultSet));
            }
        } catch (SQLException ex) {
            ex.printStackTrace();
        } finally {
            close(preparedStatement);
            close(connection);
        }
        return accounts;
    }

    @Override
    public Integer addAccount(Account account, Integer clientId) {
        Connection connection = null;
        PreparedStatement preparedStatement = null;
        ResultSet resultSet = null;
        Integer accountId = null;
        try {
            connection = daoFactory.getConnection();
            preparedStatement = connection.prepareStatement(SQL_CREATE_ACCOUNT, Statement.RETURN_GENERATED_KEYS);
            preparedStatement.setDouble(1, account.getMoney());
            preparedStatement.setInt(2, clientId);
            preparedStatement.execute();
            resultSet = preparedStatement.getGeneratedKeys();
            while (resultSet.next()) accountId = resultSet.getInt(1);
            return accountId;
        } catch (SQLException ex) {
            ex.printStackTrace();
        } finally {
            close(preparedStatement);
            close(connection);
        }
        return null;
    }

    @Override
    public boolean freeze(Integer accountId) {
        Connection connection = null;
        PreparedStatement preparedStatement = null;
        try {
            connection = daoFactory.getConnection();
            preparedStatement = connection.prepareStatement(SQL_FREEZE_ACCOUNT);
            preparedStatement.setInt(1, accountId);
            return !preparedStatement.execute();
        } catch (SQLException ex) {
            ex.printStackTrace();
        } finally {
            close(preparedStatement);
            close(connection);
        }
        return false;
    }

    @Override
    public boolean unfreeze(Integer accountId) {
        Connection connection = null;
        PreparedStatement preparedStatement = null;
        try {
            connection = daoFactory.getConnection();
            preparedStatement = connection.prepareStatement(SQL_UNFREEZE_ACCOUNT);
            preparedStatement.setInt(1, accountId);
            return !preparedStatement.execute();
        } catch (SQLException ex) {
            ex.printStackTrace();
        } finally {
            close(preparedStatement);
            close(connection);
        }
        return false;
    }

    @Override
    public boolean updateAccount(Integer accountId, Double amount) {
        Connection connection = null;
        PreparedStatement preparedStatement = null;
        try {
            connection = daoFactory.getConnection();
            preparedStatement = connection.prepareStatement(SQL_UPDATE_ACCOUNT);
            preparedStatement.setDouble(1, amount);
            preparedStatement.setInt(2, accountId);
            return !preparedStatement.execute();
        } catch (SQLException ex) {
            ex.printStackTrace();
        } finally {
            close(preparedStatement);
            close(connection);
        }
        return false;
    }

    @Override
    public List<Account> findAllFrozen() {
        Connection connection = null;
        PreparedStatement preparedStatement = null;
        ResultSet resultSet = null;
        List<Account> frozenAccounts = new ArrayList<>();
        try {
            connection = daoFactory.getConnection();
            preparedStatement = connection.prepareStatement(SQL_FIND_ALL_FROZEN);
            resultSet = preparedStatement.executeQuery();
            while (resultSet.next()) {
                frozenAccounts.add(map(resultSet));
            }
        } catch (SQLException ex) {
            ex.printStackTrace();
        } finally {
            close(preparedStatement);
            close(connection);
        }
        return frozenAccounts;
    }

    private Account map(ResultSet resultSet) throws SQLException {
        Account account = new Account();
        account.setId(resultSet.getInt(1));
        account.setMoney(resultSet.getDouble(2));
        account.setFrozen(resultSet.getBoolean(3));
        return updateAllDependences(account);
    }

    private Account updateAllDependences(Account account) throws SQLException {
        PaymentDAO paymentDAO = daoFactory.getPaymentDAO();
        CardDAO cardDAO = daoFactory.getCardDAO();
        account.setPayments(paymentDAO.findAll(account.getId()));
        account.setCards(cardDAO.findAll(account.getId()));
        return account;
    }
}
