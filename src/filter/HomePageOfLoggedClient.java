package filter;

import javax.servlet.*;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;
import java.io.IOException;

//@WebFilter(servletNames = {"Servlet"})
public class HomePageOfLoggedClient implements Filter {

    @Override
    public void doFilter(ServletRequest request, ServletResponse response, FilterChain chain)
            throws IOException, ServletException {

        HttpServletRequest req = (HttpServletRequest) request;

        HttpSession session = req.getSession(false);

        // if session exist, forward user to his welcome page
        if (session != null) {

            if (req.getPathInfo().equals("/return")) {
                req.getRequestDispatcher("/WEB-INF/view/userview/client_profile.jsp").forward(request, response);
            }
            return;
        } else req.getRequestDispatcher("/index.jsp").forward(request, response);

        chain.doFilter(request, response);
    }

    @Override
    public void init(FilterConfig filterConfig) throws ServletException {
    }

    @Override
    public void destroy() {
    }

}