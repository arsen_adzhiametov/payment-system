package action;

import org.apache.log4j.Logger;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

public class RedirectAction implements Action {

    private static final Logger LOG = Logger.getLogger(NextHistoryAction.class);

    @Override
    public String execute(HttpServletRequest request, HttpServletResponse response) throws Exception {
        LOG.info("Redirect to register page");
        return "register";
    }
}
