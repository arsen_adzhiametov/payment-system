package dao.daoInterfaces;

import model.Account;

import java.util.List;

public interface AccountDAO {

    Account findAccount(Integer id);

    List<Account> findAll(Integer clientId);

    Integer addAccount(Account account, Integer clientId);

    boolean freeze(Integer client_id);

    boolean updateAccount(Integer accountId, Double amount);

    List<Account> findAllFrozen();

    boolean unfreeze(Integer accountId);
}
