package action;

import dao.DAOFactory;
import dao.DataSourceDAOFactory;
import dao.daoInterfaces.AccountDAO;
import model.Account;
import org.apache.log4j.Logger;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

public class FreezeAction implements Action {

    private static final Logger LOG = Logger.getLogger(FreezeAction.class);

    @Override
    public String execute(HttpServletRequest request, HttpServletResponse response) throws Exception {
        DAOFactory daoFactory = DataSourceDAOFactory.getDAOFactory();

        AccountDAO accountDAO = daoFactory.getAccountDAO();

        Account account = (Account) request.getSession().getAttribute("account");
        boolean frozen = accountDAO.freeze(account.getId());
        account.setFrozen(true);
        if (frozen) {
            LOG.info("Account frozen successfully");
            return "account";
        } else {
            LOG.warn("Account was frozen already");
            request.setAttribute("error", "Ваш счет заблокировать не удалось. Попробуйте еще раз.");
            return "failed";
        }
    }
}
