package dao;

import dao.daoImplementation.*;
import dao.daoInterfaces.*;

import java.io.FileReader;
import java.io.IOException;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.util.Properties;

public class DriverManagerDAOJDBCFactory extends DAOFactory {

    private static final Properties prop = new Properties();

    static {
        try {
            prop.load(new FileReader("C:\\Users\\Kalpazan\\Dropbox\\Java\\IdeaProjects\\PaymentSystem\\dao.properties"));
        } catch (IOException ex) {
            ex.printStackTrace();
        }
    }

    @Override
    public Connection getConnection() throws SQLException {
        String driver = prop.getProperty("javabase.jdbc.driver");
        try {
            Class.forName(driver);
        } catch (ClassNotFoundException ex) {
            ex.printStackTrace();
        }
        String url = prop.getProperty("javabase.jdbc.url");
        String user = prop.getProperty("javabase.jdbc.username");
        String pass = prop.getProperty("javabase.jdbc.password");
        return DriverManager.getConnection(url, user, pass);
    }

    @Override
    public ClientDAO getClientDAO() {
        return new ClientDAOJDBC(this);
    }

    @Override
    public AccountDAO getAccountDAO() {
        return new AccountDAOJDBC(this);
    }

    @Override
    public CardDAO getCardDAO() {
        return new CardDAOJDBC(this);
    }

    @Override
    public PaymentDAO getPaymentDAO() {
        return new PaymentDAOJDBC(this);
    }

    @Override
    public AdministratorDAO getAdministratorDAO() {
        return new AdministratorDAOJDBC(this);
    }


}
