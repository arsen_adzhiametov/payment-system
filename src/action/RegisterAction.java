package action;

import dao.DAOFactory;
import dao.DataSourceDAOFactory;
import dao.daoInterfaces.ClientDAO;
import model.Client;
import org.apache.log4j.Logger;
import resources.ResourceBundleFactory;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

public class RegisterAction implements Action {

    private static final Logger LOG = Logger.getLogger(RegisterAction.class);

    @Override
    public String execute(HttpServletRequest request, HttpServletResponse response) throws Exception {

        DAOFactory daoFactory = DataSourceDAOFactory.getDAOFactory();
        ClientDAO clientDAO = daoFactory.getClientDAO();

        String name = request.getParameter("name");
        String lastName = request.getParameter("lastName");
        String email = request.getParameter("eMail");
        String password = request.getParameter("password");

        String patternEmail = "^[-a-z0-9!#$%&'*+/=?^_`{|}~]+(\\.[-a-z0-9!#$%&'*+/=?^_`{|}~]+)*@([a-z0-9]([-a-z0-9]{0,61}[a-z0-9])?\\.)*(aero|arpa|asia|biz|cat|com|coop|edu|gov|info|int|jobs|mil|mobi|museum|name|net|org|pro|tel|ru|travel|[a-z][a-z])$";
        String patternName = "^[A-za-zА-яа-я]{3,16}$";
        String patternPassword = "^[a-z0-9_-]{6,18}$";
        if (name == null || lastName == null || email == null || password == null || !email.matches(patternEmail)
                || !name.matches(patternName) || !lastName.matches(patternName) || !password.matches(patternPassword)) {
            LOG.info("Invalid register data");
            request.setAttribute("error", ResourceBundleFactory.getResourceBundle().getString("invalidData"));
            request.setAttribute("name", name);
            request.setAttribute("lastName", lastName);
            request.setAttribute("eMail", email);
            request.setAttribute("password", password);
            return "register";
        }

        Client client = new Client();
        client.setName(name);
        client.setLastName(lastName);
        client.setEmail(email);
        client.setPassword(password);
        Integer clientId = clientDAO.addClient(client);
        if (clientId != null) {
            client.setClientID(clientId);
            request.getSession().setAttribute("client", client); // Login user.
            LOG.info("Client registered and logged");

            return new AddAccountAction().execute(request, response);
        } else {
            LOG.error("Client not added to DB");
            request.setAttribute("error", ResourceBundleFactory.getResourceBundle().getString("insertClientError"));
            return "failed"; // Go back to redisplay login form with error.
        }
    }
}
