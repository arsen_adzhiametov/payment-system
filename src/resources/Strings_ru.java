package resources;

import java.util.ListResourceBundle;

public class Strings_ru extends ListResourceBundle {
    static final Object[][] contents = {
            {"register", "Регистрация"},
            {"welcome", "Привет! Только избранные могут пользоваться сервисом. Поэтому войдите или зарегистрируйтесь, пожалуйста!"},
            {"password", "Пароль"},
            {"login", "Войти"},
            {"logout", "Выйти"},
            {"success", "Успех!"},
            {"footerText", "Все права защищены и бла-бла-бла..."},
            {"myAccounts", "Мои счета"},
            {"accountNumber", "Счет №"},
            {"money", "Средств доступно:"},
            {"frozenAccount", "Счет заблокирован."},
            {"recharge", "Пополнить"},
            {"pay", "Оплатить"},
            {"history", "История"},
            {"freeze", "Заморозить"},
            {"errorMessage", "Упс! Кажется, что-то пошло не так :("},
            {"profile", "Профиль"},
            {"accounts", "Мои счета"},
            {"addAccount", "Добавить"},
            {"paymentHistory", "История платежей"},
            {"nameValidator", "Твое имя может содержать до 16 букв!"},
            {"lastnameValidator", "Фамилия тоже!"},
            {"emailValidator", "Поверьте, с почтой не обманете"},
            {"passwordValidator", "Пароль может содержать от 6 до 18 символов (буквы, цифры, тире, подчеркивание)"},
            {"ready", "Я готов!"},
            {"name", "Имя"},
            {"lastname", "Фамилия"},
            {"cards", "Карты"},
            {"return", "Хочу домой!"},
            {"cardNotFound", "Не удалось выписать кредитную карту"},
            {"clientNotFound", "Клиент не найден."},
            {"accountNotAdded", "Счет добавить не удалось."},
            {"invalidData", "Вы ввели неверные данные... Или не ввели вообще. Попробуйте еще раз"},
            {"insertClientError", "По какой-то причине вас не удалось занести в базу данных. Возможно вы уже зарегистрированы."},
            {"notFoundInDb", "По какой-то причине вас не удалось найти в базе. Возможно, вам стоит зарегистрироваться."},
            {"invalidLoginData", "Вы ввели неверную пару значений. Вернитесь и попробуйте еще раз."},
            {"unfreezeFailed", "Счет разблокировать не удалось. Попробуйте еще раз."},
            {"unfreeze", "Разморозить"}

    };

    @Override
    protected Object[][] getContents() {
        return contents;
    }
}