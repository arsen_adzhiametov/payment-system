package action;

import model.Account;
import model.Client;
import org.apache.log4j.Logger;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

public class SelectAction implements Action {

    private static final Logger LOG = Logger.getLogger(SelectAction.class);

    @Override
    public String execute(HttpServletRequest request, HttpServletResponse response) throws Exception {

        String accountID = request.getParameter("accountId");

        Account account = null;

        Client client = (Client) request.getSession().getAttribute("client");
        for (Account acc : client.getAccounts()) {
            if (acc.getId().equals(Integer.parseInt(accountID))) account = acc;
        }

        if (account != null) {
            LOG.info("Account " + accountID + " selected");
            request.getSession().setAttribute("account", account);
            return "account";
        } else {
            LOG.error("Account not found");
            request.setAttribute("error", "Не удалось найти счет. Обратитесь в главное управление.");
            return "failed";
        }
    }
}
