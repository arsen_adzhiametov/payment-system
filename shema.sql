SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0;
SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0;
SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='TRADITIONAL,ALLOW_INVALID_DATES';

CREATE SCHEMA IF NOT EXISTS `paymentsystem` DEFAULT CHARACTER SET utf8 ;
USE `paymentsystem` ;

-- -----------------------------------------------------
-- Table `paymentsystem`.`client`
-- -----------------------------------------------------
CREATE  TABLE IF NOT EXISTS `paymentsystem`.`client` (
  `id` INT(10) UNSIGNED NOT NULL AUTO_INCREMENT ,
  `name` VARCHAR(45) NOT NULL ,
  `lastname` VARCHAR(45) NOT NULL ,
  `email` VARCHAR(45) NOT NULL ,
  `password` VARCHAR(45) NOT NULL ,
  PRIMARY KEY (`id`) ,
  UNIQUE INDEX `email_UNIQUE` (`email` ASC) )
ENGINE = InnoDB
AUTO_INCREMENT = 47
DEFAULT CHARACTER SET = utf8;


-- -----------------------------------------------------
-- Table `paymentsystem`.`account`
-- -----------------------------------------------------
CREATE  TABLE IF NOT EXISTS `paymentsystem`.`account` (
  `id` INT(11) NOT NULL AUTO_INCREMENT ,
  `money` DOUBLE NOT NULL ,
  `is_frozen` TINYINT(1) NOT NULL DEFAULT '0' ,
  `client_id` INT(10) UNSIGNED NOT NULL ,
  PRIMARY KEY (`id`) ,
  INDEX `fk_account_client_idx` (`client_id` ASC) ,
  CONSTRAINT `fk_account_client`
    FOREIGN KEY (`client_id` )
    REFERENCES `paymentsystem`.`client` (`id` )
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB
AUTO_INCREMENT = 28
DEFAULT CHARACTER SET = utf8;


-- -----------------------------------------------------
-- Table `paymentsystem`.`administrator`
-- -----------------------------------------------------
CREATE  TABLE IF NOT EXISTS `paymentsystem`.`administrator` (
  `id_administrator` INT(11) NOT NULL AUTO_INCREMENT ,
  `login` VARCHAR(45) NOT NULL ,
  `password` VARCHAR(45) NOT NULL ,
  PRIMARY KEY (`id_administrator`) )
ENGINE = InnoDB
DEFAULT CHARACTER SET = utf8;


-- -----------------------------------------------------
-- Table `paymentsystem`.`card`
-- -----------------------------------------------------
CREATE  TABLE IF NOT EXISTS `paymentsystem`.`card` (
  `id_card` INT(11) NOT NULL AUTO_INCREMENT ,
  `pin_code` VARCHAR(4) NOT NULL ,
  `expires_date` DATETIME NOT NULL ,
  `account_id` INT(11) NOT NULL ,
  PRIMARY KEY (`id_card`) ,
  INDEX `fk_card_account_idx` (`account_id` ASC) ,
  CONSTRAINT `fk_card_account1`
    FOREIGN KEY (`account_id` )
    REFERENCES `paymentsystem`.`account` (`id` )
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB
AUTO_INCREMENT = 24
DEFAULT CHARACTER SET = utf8;


-- -----------------------------------------------------
-- Table `paymentsystem`.`payment`
-- -----------------------------------------------------
CREATE  TABLE IF NOT EXISTS `paymentsystem`.`payment` (
  `id_payment` INT(11) NOT NULL AUTO_INCREMENT ,
  `payment_time` TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP ,
  `amount` DOUBLE NOT NULL ,
  `action` CHAR(100) NULL DEFAULT NULL ,
  `account_id` INT(11) NOT NULL ,
  PRIMARY KEY (`id_payment`) ,
  INDEX `fk_payment_account_idx` (`account_id` ASC) ,
  CONSTRAINT `fk_payment_account1`
    FOREIGN KEY (`account_id` )
    REFERENCES `paymentsystem`.`account` (`id` )
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB
AUTO_INCREMENT = 23
DEFAULT CHARACTER SET = utf8;

USE `paymentsystem` ;


SET SQL_MODE=@OLD_SQL_MODE;
SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS;
SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS;
