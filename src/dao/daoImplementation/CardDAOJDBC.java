package dao.daoImplementation;

import dao.DAOFactory;
import dao.daoInterfaces.CardDAO;
import model.Card;

import java.sql.*;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;

import static dao.DAOJDBCUtil.close;

public class CardDAOJDBC implements CardDAO {

    private static final String SQL_CREATE_CARD = "INSERT INTO card (pin_code, expires_date, account_id) VALUES (?, ?, ?)";
    private static final String SQL_FIND_ALL_CARDS = "SELECT * FROM card WHERE account_id=(?)";
    private static final String SQL_FIND_CARD_ID_BY_ACCOUNT_ID = "SELECT id_card FROM card WHERE account_id=(?)";

    private DAOFactory daoFactory;

    public CardDAOJDBC(DAOFactory daoFactory) {
        this.daoFactory = daoFactory;
    }

    @Override
    public Card findCard(Integer id) {
        return null;
    }

    @Override
    public List<Card> findAll(Integer accountId) {
        Connection connection = null;
        PreparedStatement preparedStatement = null;
        ResultSet resultSet = null;
        List<Card> cards = new ArrayList<>();
        try {
            connection = daoFactory.getConnection();
            preparedStatement = connection.prepareStatement(SQL_FIND_ALL_CARDS);
            preparedStatement.setInt(1, accountId);
            resultSet = preparedStatement.executeQuery();
            while (resultSet.next()) {
                cards.add(map(resultSet));
            }
        } catch (SQLException ex) {
            ex.printStackTrace();
        } finally {
            close(preparedStatement);
            close(connection);
        }
        return cards;
    }

    @Override
    public Integer addCard(Card card, Integer accountId) {
        Connection connection = null;
        PreparedStatement preparedStatement = null;
        ResultSet resultSet = null;
        Integer cardId = null;
        try {
            connection = daoFactory.getConnection();
            preparedStatement = connection.prepareStatement(SQL_CREATE_CARD);
            preparedStatement.setString(1, card.getPinCode());
            preparedStatement.setDate(2, new Date(card.getExpiresDay().getTimeInMillis()));
            preparedStatement.setInt(3, accountId);
            preparedStatement.execute();
            preparedStatement = connection.prepareStatement(SQL_FIND_CARD_ID_BY_ACCOUNT_ID);
            preparedStatement.setInt(1, accountId);
            resultSet = preparedStatement.executeQuery();
            while (resultSet.next()) cardId = resultSet.getInt(1);
            return cardId;
        } catch (SQLException ex) {
            ex.printStackTrace();
        } finally {
            close(preparedStatement);
            close(connection);
        }
        return null;
    }

    private Card map(ResultSet resultSet) throws SQLException {
        Calendar calendar = Calendar.getInstance();
        Card card = new Card();
        card.setId(resultSet.getInt(1));
        card.setPinCode(resultSet.getString(2));
        calendar.setTimeInMillis(resultSet.getTimestamp(3).getTime());
        card.setExpiresDay(calendar);
        return card;
    }


}
