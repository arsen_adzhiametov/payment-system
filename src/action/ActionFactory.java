package action;

import org.apache.log4j.Logger;

import javax.servlet.http.HttpServletRequest;
import java.util.HashMap;
import java.util.Map;

public class ActionFactory {

    private static final Logger LOG = Logger.getLogger(ActionFactory.class);

    private static Map<String, Action> actions;

    static {
        actions = new HashMap();
        actions.put("POST/login", new LoginAction());
        actions.put("POST/register", new RegisterAction());
        actions.put("POST/freeze", new FreezeAction());
        actions.put("POST/select", new SelectAction());
        actions.put("POST/logout", new LogoutAction());
        actions.put("POST/redirect", new RedirectAction());
        actions.put("POST/addAccount", new AddAccountAction());
        actions.put("POST/pay", new UpdateAccountAction());
        actions.put("POST/recharge", new UpdateAccountAction());
//        actions.put("POST/history", new HistoryAction());
        actions.put("GET/return", new ReturnHomePage());
        actions.put("GET/chooseLanguage", new ChooseLanguageAction());
        actions.put("POST/showCards", new ShowCardAction());
        actions.put("POST/nextHistory", new NextHistoryAction());
        actions.put("POST/adminLogin", new AdminAuthAction());
        actions.put("POST/unfreeze", new UnfreezeAction());
    }

    public static Action getAction(HttpServletRequest request) {
        LOG.info(request.getMethod() + request.getPathInfo() + " action generated");
        return actions.get(request.getMethod() + request.getPathInfo());
    }
}
