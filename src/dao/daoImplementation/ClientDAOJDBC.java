package dao.daoImplementation;

import dao.DAOFactory;
import dao.daoInterfaces.AccountDAO;
import dao.daoInterfaces.CardDAO;
import dao.daoInterfaces.ClientDAO;
import dao.daoInterfaces.PaymentDAO;
import model.Account;
import model.Client;

import java.sql.*;
import java.util.ArrayList;
import java.util.List;

import static dao.DAOJDBCUtil.close;

public class ClientDAOJDBC implements ClientDAO {

    private static final String SQL_FIND_BY_ID = "select * from client where id = (?)";
    private static final String SQL_FIND_ALL = "select * from client";
    private static final String SQL_INSERT_CLIENT = "INSERT INTO client (name, lastname, email, password) VALUES (?, ?, ?, ?)";
    private static final String SQL_FIND_BY_LOGIN_AND_PASS = "SELECT * FROM client WHERE email=(?) AND password=(?)";

    private DAOFactory daoFactory;

    public ClientDAOJDBC(DAOFactory daoFactory) {
        this.daoFactory = daoFactory;
    }

    @Override
    public Client findClient(Integer clientId) {
        Connection connection = null;
        PreparedStatement preparedStatement = null;
        ResultSet resultSet = null;
        Client client = null;

        try {
            connection = daoFactory.getConnection();
            preparedStatement = connection.prepareStatement(SQL_FIND_BY_ID);
            preparedStatement.setInt(1, clientId);
            resultSet = preparedStatement.executeQuery();
            while (resultSet.next()) {
                client = map(resultSet);
            }

        } catch (SQLException e) {
            e.printStackTrace();
        } finally {
            close(resultSet);
            close(preparedStatement);
            close(connection);
        }
        return client;
    }

    @Override
    public List<Client> findAll() {
        Connection connection = null;
        PreparedStatement preparedStatement = null;
        ResultSet resultSet = null;
        List<Client> clients = new ArrayList();

        try {
            connection = daoFactory.getConnection();
            preparedStatement = connection.prepareStatement(SQL_FIND_ALL);
            resultSet = preparedStatement.executeQuery();
            while (resultSet.next()) {
                clients.add(map(resultSet));
            }
        } catch (SQLException e) {
            e.printStackTrace();
        } finally {
            close(resultSet);
            close(preparedStatement);
            close(connection);
        }
        return clients;
    }

    @Override
    public Integer addClient(Client client) {
        Connection connection = null;
        PreparedStatement preparedStatement = null;
        ResultSet resultSet = null;
        Integer client_id = null;
        try {
            connection = daoFactory.getConnection();
            preparedStatement = connection.prepareStatement(SQL_INSERT_CLIENT, Statement.RETURN_GENERATED_KEYS);
            preparedStatement.setString(1, client.getName());
            preparedStatement.setString(2, client.getLastName());
            preparedStatement.setString(3, client.getEmail());
            preparedStatement.setString(4, client.getPassword());
            preparedStatement.execute();
            resultSet = preparedStatement.getGeneratedKeys();
            while (resultSet.next()) client_id = resultSet.getInt(1);
            return client_id;
        } catch (SQLException ex) {
            ex.printStackTrace();
        } finally {
            close(preparedStatement);
            close(connection);
        }
        return null;
    }

    @Override
    public Client find(String email, String password) {
        Connection connection = null;
        PreparedStatement preparedStatement = null;
        ResultSet resultSet = null;
        Client client = null;
        try {
            connection = daoFactory.getConnection();
            preparedStatement = connection.prepareStatement(SQL_FIND_BY_LOGIN_AND_PASS);
            preparedStatement.setString(1, email);
            preparedStatement.setString(2, password);
            resultSet = preparedStatement.executeQuery();
            while (resultSet.next()) {
                client = map(resultSet);
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return client;
    }

    private Client map(ResultSet resultSet) throws SQLException {
        Client client = new Client();
        client.setClientID(resultSet.getInt(1));
        client.setName(resultSet.getString(2));
        client.setLastName(resultSet.getString(3));
        client.setEmail(resultSet.getString(4));
        client.setPassword(resultSet.getString(5));
        return updateAllDependences(client);
    }

    private Client updateAllDependences(Client client) throws SQLException {
        AccountDAO accountDAO = daoFactory.getAccountDAO();
        PaymentDAO paymentDAO = daoFactory.getPaymentDAO();
        CardDAO cardDAO = daoFactory.getCardDAO();

        client.setAccounts(accountDAO.findAll(client.getClientID()));

        for (Account account : client.getAccounts()) {
            account.setPayments(paymentDAO.findAll(account.getId()));
            account.setCards(cardDAO.findAll(account.getId()));
        }

        return client;
    }

}
