package dao.daoInterfaces;

import model.Card;

import java.util.List;

public interface CardDAO {

    Card findCard(Integer id);

    List<Card> findAll(Integer accountId);

    Integer addCard(Card card, Integer accountId);

}
