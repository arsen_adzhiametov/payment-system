package action;

import dao.DAOFactory;
import dao.DataSourceDAOFactory;
import dao.daoInterfaces.PaymentDAO;
import model.Account;
import model.Payment;
import org.apache.log4j.Logger;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.util.List;

public class NextHistoryAction implements Action {

    private static final Logger LOG = Logger.getLogger(NextHistoryAction.class);

    @Override
    public String execute(HttpServletRequest request, HttpServletResponse response) throws Exception {
        DAOFactory daoFactory = DataSourceDAOFactory.getDAOFactory();
        PaymentDAO paymentDAO = daoFactory.getPaymentDAO();

        Account account = (Account) request.getSession().getAttribute("account");
        account.getPayments().clear();

        String lastShownPaymentsId = request.getParameter("lastShownPaymentsId");
        if (lastShownPaymentsId == null && lastShownPaymentsId.equals("")) {
            request.setAttribute("error", "Больше ничего нет :(");
            LOG.warn("NPE in last shown history");
            return "failed";
        }

        List<Payment> payments = paymentDAO.findFew(account.getId(), Integer.parseInt(lastShownPaymentsId));

        account.getPayments().addAll(payments);

        if (payments != null) {
            request.setAttribute("account", account);
            if (payments.size() == 0) {
                request.setAttribute("error", "Ничего беольше нет!");
                LOG.info("No more history. All are shown");
            }
            return "history";
        } else {
            request.setAttribute("error", "Не удалось заглянуть в журнал");
            LOG.error("Payments not loaded");
            return "failed";
        }
    }
}
