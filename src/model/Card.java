package model;

import java.io.Serializable;
import java.util.Calendar;

public class Card implements Serializable {
    private Integer id;
    private String pinCode;
    private Calendar expiresDay;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getPinCode() {
        return pinCode;
    }

    public void setPinCode(String pinCode) {
        this.pinCode = pinCode;
    }

    public Calendar getExpiresDay() {
        return expiresDay;
    }

    public void setExpiresDay(Calendar expiresDay) {
        expiresDay.add(Calendar.MONTH, 1);
        this.expiresDay = expiresDay;
    }

    @Override
    public String toString() {
        return "Card #"+getId();
    }
}
