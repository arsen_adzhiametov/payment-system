package dao.daoInterfaces;

import model.Client;

import java.util.List;

public interface ClientDAO {

    Client findClient(Integer id);
    List<Client> findAll();
    Integer addClient(Client client);
    Client find(String email, String password);
}
