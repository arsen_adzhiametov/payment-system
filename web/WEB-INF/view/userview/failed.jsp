<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>


<c:set var='view' value='failed' scope='session'/>

<div id="centerColumn">
    <img src="/res/cc_login_warning.png">

    <h3 class="message"><fmt:message key="errorMessage"/>
    </h3>

    ${error}

</div>
