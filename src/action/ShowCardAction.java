package action;

import model.Account;
import model.Card;
import org.apache.log4j.Logger;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.util.List;

public class ShowCardAction implements Action {

    private static final Logger LOG = Logger.getLogger(ShowCardAction.class);

    @Override
    public String execute(HttpServletRequest request, HttpServletResponse response) throws Exception {

        Account account = (Account) request.getSession().getAttribute("account");

        List<Card> cards = account.getCards();

        if (cards != null) {
            LOG.info("Cards showed");
            return "cards";
        } else {
            request.setAttribute("error", "Не удалось найти ни одной карты. Простите.");
            LOG.error("Cards not found");
            return "failed";
        }
    }
}
