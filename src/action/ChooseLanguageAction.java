package action;

import org.apache.log4j.Logger;
import resources.ResourceBundleFactory;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.util.Locale;

public class ChooseLanguageAction implements Action {

    private static final Logger LOG = Logger.getLogger(ChooseLanguageAction.class);

    @Override
    public String execute(HttpServletRequest request, HttpServletResponse response) throws Exception {

        String language = request.getParameter("language");
        ResourceBundleFactory.setLocale(new Locale(language));

        String userView = (String) request.getSession().getAttribute("view");

//        Account account = (Account) request.getSession().getAttribute("account");

        request.setAttribute("language", language);
//        request.getSession().setAttribute("account", account);
        LOG.info("Locale changed to " + language);
        return userView;
    }
}
