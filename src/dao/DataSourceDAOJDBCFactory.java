package dao;

import dao.daoImplementation.*;
import dao.daoInterfaces.*;

import javax.naming.Context;
import javax.naming.InitialContext;
import javax.naming.NamingException;
import javax.sql.DataSource;
import java.sql.Connection;
import java.sql.SQLException;

public class DataSourceDAOJDBCFactory extends DAOFactory {

    @Override
    public Connection getConnection() throws SQLException {
        Context context = null;
        DataSource ds = null;
        try {
            context = new InitialContext();
            ds = (DataSource) context.lookup("java:comp/env/jdbc/paymentsystem");
        } catch (NamingException e) {
            e.printStackTrace();
        }

//        MysqlDataSource ds = new MysqlDataSource();
//        ds.setServerName("localhost");
//        ds.setPortNumber(3306);
//        ds.setDatabaseName("mypaypal");
//        ds.setUser("root");
//        ds.setPassword("root");

        return ds.getConnection();
    }

    @Override
    public ClientDAO getClientDAO() {
        return new ClientDAOJDBC(this);
    }

    @Override
    public AccountDAO getAccountDAO() {
        return new AccountDAOJDBC(this);
    }

    @Override
    public CardDAO getCardDAO() {
        return new CardDAOJDBC(this);
    }

    @Override
    public PaymentDAO getPaymentDAO() {
        return new PaymentDAOJDBC(this);
    }

    @Override
    public AdministratorDAO getAdministratorDAO() {
        return new AdministratorDAOJDBC(this);
    }


}
