package action;

import dao.DAOFactory;
import dao.DataSourceDAOFactory;
import dao.daoInterfaces.PaymentDAO;
import model.Account;
import model.Payment;
import org.apache.log4j.Logger;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.util.List;

public class HistoryAction implements Action {

    private static final Logger LOG = Logger.getLogger(HistoryAction.class);

    @Override
    public String execute(HttpServletRequest request, HttpServletResponse response) throws Exception {

        DAOFactory daoFactory = DataSourceDAOFactory.getDAOFactory();
        PaymentDAO paymentDAO = daoFactory.getPaymentDAO();

        Account account = (Account) request.getSession().getAttribute("account");

        List<Payment> payments = paymentDAO.findFew(account.getId(), 0);

        if (payments != null) {
            LOG.info("Payments loaded");
            request.setAttribute("payments", payments);
            return "history";
        } else {
            LOG.warn("Payments not loaded");
            request.setAttribute("error", "Не удалось заглянуть в журнал. Простите.");
            return "failed";
        }
    }
}
