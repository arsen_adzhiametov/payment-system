package action;

import model.Administrator;
import model.Client;
import org.apache.log4j.Logger;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

public class ReturnHomePage implements Action {

    private static final Logger LOG = Logger.getLogger(ReturnHomePage.class);

    @Override
    public String execute(HttpServletRequest request, HttpServletResponse response) throws Exception {
        HttpSession session = request.getSession(false);
        Administrator administrator = (Administrator) session.getAttribute("administrator");
        Client client = (Client) session.getAttribute("client");

        if (administrator != null) return "unfreeze";
        if (client != null) {
            LOG.info("Returned to client profile");
            return "client_profile";
        }
        LOG.info("Returned to index");
        return "index";
    }
}