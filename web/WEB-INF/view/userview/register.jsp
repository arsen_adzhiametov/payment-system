<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>

<c:set var='view' value='register' scope='session'/>

<div id="centerColumn">

    <form method="POST" action="/paymentsystem/register" enctype="multipart/form-data; charset=utf-8">
        <table width="100%" border="0" cellspacing="0" cellpadding="10" class="reg_form">
            <tr>
                <td width="30%">&nbsp;</td>
                <td width="30%">&nbsp;</td>
                <td width="40%">&nbsp;</td>
            </tr>
            <tr>
                <td><fmt:message key="name"/></td>
                <td><input type="text" name="name" value="${name}"></td>
                <jsp:text>${error}</jsp:text>
                <td><fmt:message key="nameValidator"/></td>
            </tr>
            <tr>
                <td><fmt:message key="lastname"/></td>
                <td><input type="text" name="lastName" value="${lastName}"></td>
                <td><fmt:message key="lastnameValidator"/></td>
            </tr>
            <tr>
                <td>e-mail</td>
                <td><input type="text" name="eMail" value="${eMail}"></td>
                <td><fmt:message key="emailValidator"/></td>
            </tr>
            <tr>
                <td><fmt:message key="password"/></td>
                <td><input type="text" name="password" value="${password}"></td>
                <td><fmt:message key="passwordValidator"/></td>
            </tr>
            <tr>
                <td>&nbsp;</td>
                <td>
                    <button class="button"><fmt:message key="ready"/></button>
                </td>
            </tr>

        </table>
    </form>

</div>