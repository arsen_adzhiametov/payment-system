package action;

import org.apache.log4j.Logger;
import resources.ResourceBundleFactory;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

public class LogoutAction implements Action {

    private static final Logger LOG = Logger.getLogger(LogoutAction.class);

    @Override
    public String execute(HttpServletRequest request, HttpServletResponse response) throws Exception {
        request.getSession().invalidate();
        ResourceBundleFactory.setLocale(request.getLocale());
        LOG.info("Logout: session invalidate");
        return "index"; // Redirect to home page.
    }
}
