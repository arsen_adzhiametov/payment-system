package controller;

import action.Action;
import action.ActionFactory;
import org.apache.log4j.Logger;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

public class ControllerServlet extends HttpServlet {

    private static final Logger LOG = Logger.getLogger(ControllerServlet.class);

    @Override
    public void init() throws ServletException {
        super.init();
        System.setProperty("file.encoding", "utf-8");
    }

    protected void processRequest(HttpServletRequest request, HttpServletResponse response) throws javax.servlet.ServletException, IOException {
        try {
            Action action = ActionFactory.getAction(request);
            String view = action.execute(request, response);
            LOG.info(view + " view responded");
            dispatch(request, response, view);
//            if (view.equals(request.getPathInfo().substring(1))) {
//            request.getRequestDispatcher("/WEB-INF/view/" + view + ".jsp").forward(request, response);
//            } else {
//                response.sendRedirect(view);
//            }
        } catch (Exception e) {
            LOG.error("controller error: " + e.getMessage());
            throw new ServletException("Executing action failed.", e);
        }
    }

    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws javax.servlet.ServletException, IOException {
        processRequest(request, response);
    }

    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws javax.servlet.ServletException, IOException {
        processRequest(request, response);
    }

    private void dispatch(HttpServletRequest request, HttpServletResponse response, String view) throws ServletException, IOException {

        String prefix = "/WEB-INF/view/userview/";
        String suffix = ".jsp";
//        if (view.equals("client_profile")) {
//            response.sendRedirect("Servlet");
//            return;
//        }
        if (view.equals("index")) {
            view = "/" + view + suffix;
        } else {
            view = prefix + view + suffix;
        }
        RequestDispatcher dispatcher = request.getRequestDispatcher(view);
        dispatcher.forward(request, response);
    }
}
