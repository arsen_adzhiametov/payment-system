<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ taglib uri="/WEB-INF/tag/mytaglib.tld" prefix="t" %>
<html>
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <link rel="stylesheet" type="text/css" href="<c:url value="/css/payment_system.css"/>">
    <title><t:appName/></title>
</head>

<body id="main">

<div id="center">
    <form method="post" action="/admin/adminLogin">
        <table width="100%" border="0" cellspacing="0" cellpadding="10" class="admin_login_form">
            ${error}
            <tr>
                <td width="35%">login</td>
                <td width="65%"><input type="text" name="login" value="${login}"></td>
            </tr>
            <tr>
                <td>password</td>
                <td><input type="password" name="password"></td>
            </tr>
            <tr>
                <td>&nbsp;</td>
                <td>
                    <button class="button">Auth</button>
                </td>
            </tr>

        </table>
    </form>

</div>

</body>
</html>