package tag;

import javax.servlet.jsp.JspException;
import javax.servlet.jsp.JspWriter;
import javax.servlet.jsp.tagext.SimpleTagSupport;
import java.io.IOException;

public class AppNameTag extends SimpleTagSupport {
    public void doTag() throws JspException {
        try {
            JspWriter out = getJspContext().getOut();
            out.write("Payment System");
        } catch (IOException e) {
            throw new JspException(e.getMessage());
        }

    }
}
