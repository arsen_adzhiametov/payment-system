<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>

<c:set var='view' value='success' scope='session'/>

<div id="centerColumn">
    <img src="/res/cc_slider_accept.png">

    <h3 class="message"><fmt:message key="success"/>
    </h3>

    ${success}

</div>