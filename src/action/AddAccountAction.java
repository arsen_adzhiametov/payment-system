package action;

import dao.DAOFactory;
import dao.DataSourceDAOFactory;
import dao.daoInterfaces.AccountDAO;
import dao.daoInterfaces.CardDAO;
import model.Account;
import model.Card;
import model.Client;
import org.apache.log4j.Logger;
import resources.ResourceBundleFactory;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.util.Calendar;

public class AddAccountAction implements Action {

    private static final Logger LOG = Logger.getLogger(AddAccountAction.class);

    private static final double MONEY_ON_CREATE = 20;

    @Override
    public String execute(HttpServletRequest request, HttpServletResponse response) throws Exception {

        DAOFactory daoFactory = DataSourceDAOFactory.getDAOFactory();
        AccountDAO accountDAO = daoFactory.getAccountDAO();
        CardDAO cardDAO = daoFactory.getCardDAO();

        Client client = (Client) request.getSession().getAttribute("client");
        if (client == null) {
            LOG.error("Client is not in session!");
            request.setAttribute("error", ResourceBundleFactory.getResourceBundle().getString("clientNotFound"));
            return "failed";
        }

        Account account = new Account();
        account.setMoney(MONEY_ON_CREATE);
        Integer accountId = accountDAO.addAccount(account, client.getClientID());
        client.getAccounts().add(account);    //лучше считать снова
        if (accountId == null) {
            LOG.error("Account not added");
            request.setAttribute("error", ResourceBundleFactory.getResourceBundle().getString("accountNotAdded"));
            return "failed";
        }

        account.setId(accountId);

        Card card = new Card();
        card.setPinCode("000");
        card.setExpiresDay(Calendar.getInstance());
        Integer cardId = cardDAO.addCard(card, account.getId());
        account.getCards().add(card);

        if (cardId != null) {
            card.setId(cardId);
            request.getSession().setAttribute("account", account);
            LOG.info("Account added successfully!");
            return new PaymentAction().execute(request, response);
        } else {
            LOG.error("Card not added");
            request.setAttribute("error", ResourceBundleFactory.getResourceBundle().getString("cardNotFound"));
            return "failed";
        }
    }
}

