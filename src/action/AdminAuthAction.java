package action;

import dao.DAOFactory;
import dao.DataSourceDAOFactory;
import dao.daoInterfaces.AccountDAO;
import dao.daoInterfaces.AdministratorDAO;
import model.Account;
import model.Administrator;
import org.apache.log4j.Logger;
import resources.ResourceBundleFactory;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.util.List;

public class AdminAuthAction implements Action {

    private static final Logger LOG = Logger.getLogger(AdminAuthAction.class);

    @Override
    public String execute(HttpServletRequest request, HttpServletResponse response) throws Exception {
        DAOFactory daoFactory = DataSourceDAOFactory.getDAOFactory();
        AdministratorDAO administratorDAO = daoFactory.getAdministratorDAO();
        AccountDAO accountDAO = daoFactory.getAccountDAO();

        String login = request.getParameter("login");
        String password = request.getParameter("password");
        if (login == null || password == null) {
            request.setAttribute("error", ResourceBundleFactory.getResourceBundle().getString("invalidLoginData"));
            request.setAttribute("email", login);
            LOG.info("Invalid login data");
            return "admin";
        }

        Administrator administrator = administratorDAO.getAdmin(login, password);

        if (administrator != null) {
            request.getSession().invalidate();
            List<Account> frozenAccounts = accountDAO.findAllFrozen();
            request.getSession().setAttribute("frozenAccounts", frozenAccounts);
            request.getSession().setAttribute("administrator", administrator);
            return "unfreeze";
        } else {
            LOG.error("Admin not found");
            request.setAttribute("error", ResourceBundleFactory.getResourceBundle().getString("notFoundInDb")); // Store error message in request scope.
            return "admin"; // Go back to redisplay login form with error.
        }
    }
}
