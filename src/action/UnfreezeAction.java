package action;

import dao.DAOFactory;
import dao.DataSourceDAOFactory;
import dao.daoInterfaces.AccountDAO;
import org.apache.log4j.Logger;
import resources.ResourceBundleFactory;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

public class UnfreezeAction implements Action {

    private static final Logger LOG = Logger.getLogger(UnfreezeAction.class);

    @Override
    public String execute(HttpServletRequest request, HttpServletResponse response) throws Exception {
        DAOFactory daoFactory = DataSourceDAOFactory.getDAOFactory();
        AccountDAO accountDAO = daoFactory.getAccountDAO();

        String accountId = request.getParameter("accountId");
        if (accountId == null) return "failed";

        boolean unfrozen = accountDAO.unfreeze(Integer.parseInt(accountId));

        if (unfrozen) {
            LOG.info("Account unfrozen successfully");
            request.getSession().setAttribute("frozenAccounts", accountDAO.findAllFrozen());
            return "unfreeze";
        } else {
            LOG.warn("Account do not unfrozen");
            request.setAttribute("error", ResourceBundleFactory.getResourceBundle().getString("unfreezeFailed"));
            return "failed";
        }
    }
}
