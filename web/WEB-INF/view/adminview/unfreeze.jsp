<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ taglib uri="/WEB-INF/tag/mytaglib.tld" prefix="t" %>

<c:set var='view' value='unfreeze' scope='session'/>


<c:set var="language"
       value="${not empty param.language ? param.language : not empty language ? language : pageContext.request.locale}"
       scope="session"/>
<fmt:setLocale value="${language}"/>
<fmt:setBundle basename="resources.Strings"/>

<html lang="${language}">

<head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <link rel="stylesheet" type="text/css" href="<c:url value="/css/payment_system.css"/>">
    <title>Payment System</title>
</head>

<body id="main">
<%--Set language based on user's choice--%>
<c:if test="${!empty language}">
    <fmt:setLocale value="${language}" scope="session"/>
</c:if>

<div id="header">
    <div id="topHeader">
        <div id="widgetBar">
            <%--<form method="POST" action="/paymentsystem/chooseLanguage">--%>
            <%--<select id="language" name="language" onchange="submit()">--%>
            <%--<option value="en" ${language == 'en' ? 'selected' : ''}>English</option>--%>
            <%--<option value="ru" ${language == 'ru' ? 'selected' : ''}>Русский</option>--%>
            <%--</select>--%>
            <%--</form>--%>
            <%--language selection widget--%>
            <c:choose>
                <%-- When user hasn't explicitly set language,
                render toggle according to browser's preferred locale --%>
                <c:when test="${empty sessionScope['javax.servlet.jsp.jstl.fmt.locale.session']}">
                    <c:choose>
                        <c:when test="${pageContext.request.locale.language ne 'ru'}">
                            EN
                        </c:when>
                        <c:otherwise>
                            <c:url var="url" value="/admin/chooseLanguage">
                                <c:param name="language" value="en"/>
                            </c:url>
                            <div class="bubble"><a href="${url}">EN</a></div>
                        </c:otherwise>
                    </c:choose> |

                    <c:choose>
                        <c:when test="${pageContext.request.locale.language eq 'ru'}">
                            RU
                        </c:when>
                        <c:otherwise>
                            <c:url var="url" value="/admin/chooseLanguage">
                                <c:param name="language" value="ru"/>
                            </c:url>
                            <div class="bubble"><a href="${url}">RU</a></div>
                        </c:otherwise>
                    </c:choose>
                </c:when>

                <%-- Otherwise, render widget according to the set locale --%>
                <c:otherwise>
                    <c:choose>
                        <c:when test="${sessionScope['javax.servlet.jsp.jstl.fmt.locale.session'] ne 'ru'}">
                            EN
                        </c:when>
                        <c:otherwise>
                            <c:url var="url" value="/admin/chooseLanguage">
                                <c:param name="language" value="en"/>
                            </c:url>
                            <div class="bubble"><a href="${url}">EN</a></div>
                        </c:otherwise>
                    </c:choose> |

                    <c:choose>
                        <c:when test="${sessionScope['javax.servlet.jsp.jstl.fmt.locale.session'] eq 'ru'}">
                            RU
                        </c:when>
                        <c:otherwise>
                            <c:url var="url" value="/admin/chooseLanguage">
                                <c:param name="language" value="ru"/>
                            </c:url>
                            <div class="bubble"><a href="${url}">RU</a></div>
                        </c:otherwise>
                    </c:choose>
                </c:otherwise>
            </c:choose>

            <br>
            <br>
            <c:if test="${sessionScope.administrator!=null}">
                <form method="POST" action="/paymentsystem/logout">
                    <button><fmt:message key="logout"/></button>
                </form>
            </c:if>
        </div>

        <div id="logo">
            <img src="/res/logo.jpg" alt="Payment System logo">
        </div>

        <div id="logoText" class="logoText">
            <t:appName/>
        </div>
    </div>
    <div id="centerColumn">

        <h3><fmt:message key="unfreeze"/><br> <br>

            <div id="account_layout" style="overflow-y: scroll; height: 250px">

                <table>
                    <c:forEach var="account" items="${frozenAccounts}">
                        <tr>
                            <td>
                                <form method="post" action="/admin/unfreeze">
                                    <input type="hidden" name="accountId" value="${account.id}"/>
                                    <button><fmt:message key="accountNumber"/> ${account.id}</button>
                                </form>
                            </td>
                        </tr>
                    </c:forEach>

                </table>

            </div>

        </h3>

    </div>
    <div id="footer">
        <img src="/res/footerLine.png"> <br>
        <fmt:message key="footerText"/>
    </div>
</div>
</body>

</html>