package resources;

import java.util.Locale;
import java.util.ResourceBundle;

public class ResourceBundleFactory {

    private static Locale locale = new Locale("en");

    public static ResourceBundle getResourceBundle() {
        ResourceBundle resourceBundle = ResourceBundle.getBundle("resources.Strings", locale);
        return resourceBundle;
    }

    public static Locale getLocale() {
        return locale;
    }

    public static void setLocale(Locale locale) {
        ResourceBundleFactory.locale = locale;
    }
}
