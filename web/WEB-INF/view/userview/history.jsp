<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>

<c:set var='view' value='history' scope='session'/>

<div id="centerColumn">

    <h3><fmt:message key="paymentHistory"/>
    </h3>
    ${error}
    <table width="100%" border="0" cellspacing="0" cellpadding="10">
        <tr>
            <td width="33%">Действие</td>
            <td width="33%">Сумма</td>
            <td width="33%">Время и дата</td>
        </tr>
        <c:forEach var="payment" items="${account.payments}">

            <tr>
                <td>${payment.action}</td>
                <td> ${payment.amount} USD</td>
                <td>${payment.time.getTime()} </td>
            </tr>
        </c:forEach>

    </table>
    <form method="POST" action="/paymentsystem/nextHistory">
        <c:if test="${account.payments != null && account.payments.size()!=0}">
            <input hidden="hidden" name="lastShownPaymentsId"
                   value="${account.payments.get(account.payments.size()-1).id}">
            <input type="submit" value=">">
        </c:if>
    </form>

</div>