package dao.daoImplementation;

import dao.DAOFactory;
import dao.daoInterfaces.PaymentDAO;
import model.Payment;

import java.sql.*;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;

import static dao.DAOJDBCUtil.close;

public class PaymentDAOJDBC implements PaymentDAO {

    private static final String SQL_CREATE_PAYMENT = "INSERT INTO payment (payment_time, amount, action, account_id) VALUES (?, ?, ?, ?)";
    private static final String SQL_FIND_ALL_PAYMENTS = "SELECT * FROM payment WHERE account_id = ?";
    private static final String SQL_FIND_FEW_PAYMENTS = "SELECT * FROM payment WHERE account_id = ? AND id_payment > ? LIMIT 5";

    private DAOFactory daoFactory;

    public PaymentDAOJDBC(DAOFactory daoFactory) {
        this.daoFactory = daoFactory;
    }

    @Override
    public Payment findPayment(Integer id) {
        return null;
    }

    @Override
    public List<Payment> findAll(Integer accountId) {
        Connection connection = null;
        PreparedStatement preparedStatement = null;
        ResultSet resultSet = null;
        List<Payment> payments = new ArrayList<>();
        try {
            connection = daoFactory.getConnection();
            preparedStatement = connection.prepareStatement(SQL_FIND_ALL_PAYMENTS);
            preparedStatement.setInt(1, accountId);
            resultSet = preparedStatement.executeQuery();
            while (resultSet.next()) {
                payments.add(map(resultSet));
            }
        } catch (SQLException ex) {
            ex.printStackTrace();
        } finally {
            close(preparedStatement);
            close(connection);
        }
        return payments;
    }

    @Override
    public boolean addPayment(Payment payment, Integer accountId) {
        Connection connection = null;
        PreparedStatement preparedStatement = null;
        try {
            connection = daoFactory.getConnection();
            preparedStatement = connection.prepareStatement(SQL_CREATE_PAYMENT);
            preparedStatement.setTimestamp(1, new Timestamp(payment.getTime().getTimeInMillis()));
            preparedStatement.setDouble(2, payment.getAmount());
            preparedStatement.setString(3, payment.getAction());
            preparedStatement.setInt(4, accountId);
            return !preparedStatement.execute();
        } catch (SQLException ex) {
            ex.printStackTrace();
        } finally {
            close(preparedStatement);
            close(connection);
        }
        return false;
    }

    @Override
    public List<Payment> findFew(Integer accountId, int lastId) {
        Connection connection = null;
        PreparedStatement preparedStatement = null;
        ResultSet resultSet = null;
        List<Payment> payments = new ArrayList<>();
        try {
            connection = daoFactory.getConnection();
            preparedStatement = connection.prepareStatement(SQL_FIND_FEW_PAYMENTS);
            preparedStatement.setInt(1, accountId);
            preparedStatement.setInt(2, lastId);
            resultSet = preparedStatement.executeQuery();
            while (resultSet.next()) {
                payments.add(map(resultSet));
            }
        } catch (SQLException ex) {
            ex.printStackTrace();
        } finally {
            close(preparedStatement);
            close(connection);
        }
        return payments;
    }

    private Payment map(ResultSet resultSet) throws SQLException {
        Calendar calendar = Calendar.getInstance();
        Payment payment = new Payment();
        payment.setId(resultSet.getInt(1));
        calendar.setTimeInMillis(resultSet.getTimestamp(2).getTime());
        payment.setTime(calendar);
        payment.setAction(resultSet.getString(4));
        payment.setAmount(resultSet.getDouble(3));
        return payment;
    }

}
