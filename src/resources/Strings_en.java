package resources;

import java.util.ListResourceBundle;

public class Strings_en extends ListResourceBundle {
    static final Object[][] contents = {
            {"register", "Register"},
            {"welcome", "Hi! Only a select few have access to the service. Therefore login or register, please."},
            {"password", "Password"},
            {"login", "Login"},
            {"logout", "Logout"},
            {"success", "Success!"},
            {"footerText", "All rights reserved and blah blah blah ..."},
            {"myAccounts", "My accounts"},
            {"accountNumber", "Account #"},
            {"money", "Money available:"},
            {"frozenAccount", "Account blocked."},
            {"recharge", "Recharge"},
            {"pay", "Pay"},
            {"history", "History"},
            {"freeze", "Freeze"},
            {"errorMessage", "Oops! It seems that something went wrong :("},
            {"profile", "Profile"},
            {"accounts", "My accounts"},
            {"addAccount", "Add"},
            {"paymentHistory", "Payments history"},
            {"nameValidator", "Your name can contain up to 16 letters!"},
            {"lastnameValidator", "Last name, too."},
            {"emailValidator", "Believe me, you will not fool the mail."},
            {"passwordValidator", "Password 6 to 18 characters (letters, numbers, underscores and dashes)"},
            {"ready", "I am ready!"},
            {"name", "Name"},
            {"lastname", "Lastname"},
            {"cards", "Cards"},
            {"return", "Return to homepage"},
            {"cardNotFound", "Sorry, card not found."},
            {"clientNotFound", "Client not found."},
            {"accountNotAdded", "Account not added."},
            {"invalidData", "Incorrect data ... Or not entered at all. Try again"},
            {"insertClientError", "For some reason you could not bring in a database. Perhaps you are already a member."},
            {"notFoundInDb", "For some reason you can not find in the database. Maybe you should register."},
            {"invalidLoginData", "You entered an invalid value pair. Go back and try again."},
            {"unfreezeFailed", "The account failed to unlock. Try again."},
            {"unfreeze", "Unfreeze"}

    };

    @Override
    protected Object[][] getContents() {
        return contents;
    }
}
