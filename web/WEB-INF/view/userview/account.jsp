<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>

<c:set var='view' value='account' scope='session'/>

<div id="indexLeftColumn">
    <h3><fmt:message key="myAccounts"/></h3>

    <fmt:message key="accountNumber"/> ${account.id} <br>
    <fmt:message key="money"/> ${account.money} USD <br> <br>
    <c:if test="${account.frozen}"> <fmt:message key="frozenAccount"/> </c:if>

</div>

${error}

<h3 id="indexRightColumn">

    <table width="100%" border="0" cellspacing="0" cellpadding="10">
        <tr>
            <form method="POST" action="/paymentsystem/recharge">
                <td><input type="text" name="amount" <c:if test="${account.frozen}"> disabled="disabled" </c:if>></td>
                <td>
                    <button class="button" <c:if test="${account.frozen}"> disabled="disabled" </c:if> ><fmt:message
                            key="recharge"/></button>
                </td>

            </form>
        </tr>
        <tr>
            <form name="pay_form" method="POST" action="/paymentsystem/pay">
                <td><input type="text" formaction="POST" name="amount" <c:if test="${account.frozen}">
                           disabled="disabled" </c:if>></td>
                <td>
                    <button class="button" <c:if test="${account.frozen}"> disabled="disabled" </c:if> ><fmt:message
                            key="pay"/></button>
                </td>

            </form>
        </tr>
        <tr>
            <form method="POST" action="/paymentsystem/nextHistory">
                <td>
                    <input hidden="hidden" name="lastShownPaymentsId" value="0">
                    <button class="button"><fmt:message key="history"/></button>
                </td>
            </form>
        </tr>
        <tr>
            <form method="POST" action="/paymentsystem/showCards">
                <td>
                    <button class="button"><fmt:message key="cards"/></button>
                </td>
            </form>
        </tr>
        <tr>
            <form method="POST" action="/paymentsystem/freeze">
                <td>
                    <button class="button" <c:if test="${account.frozen}"> disabled="disabled" </c:if> ><fmt:message
                            key="freeze"/></button>
                </td>
            </form>
        </tr>
    </table>
</h3>
