package action;

import dao.DAOFactory;
import dao.DataSourceDAOFactory;
import dao.daoInterfaces.ClientDAO;
import model.Client;
import org.apache.log4j.Logger;
import resources.ResourceBundleFactory;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

public class LoginAction implements Action {

    private static final Logger LOG = Logger.getLogger(LoginAction.class);

    @Override
    public String execute(HttpServletRequest request, HttpServletResponse response) throws Exception {
        DAOFactory daoFactory = DataSourceDAOFactory.getDAOFactory();
        ClientDAO clientDAO = daoFactory.getClientDAO();

        String email = request.getParameter("email");
        String password = request.getParameter("password");
        if (email == null || password == null || !email.contains("@")) {
            request.setAttribute("error", ResourceBundleFactory.getResourceBundle().getString("invalidLoginData"));
            request.setAttribute("email", email);
            LOG.info("Invalid login data");
            return "index";
        }

        Client client = clientDAO.find(email, password);

        if (client != null) {
            request.getSession().setAttribute("client", client); // Login user.
            return "client_profile";
        } else {
            LOG.error("Client not found");
            request.setAttribute("error", ResourceBundleFactory.getResourceBundle().getString("notFoundInDb")); // Store error message in request scope.
            return "index"; // Go back to redisplay login form with error.
        }
    }
}
