package dao.daoInterfaces;

import model.Administrator;

public interface AdministratorDAO {
    Administrator getAdmin(String login, String password);
}
