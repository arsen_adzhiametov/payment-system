<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>

<c:set var='view' value='index' scope='session'></c:set>

<c:if test="${sessionScope.client!=null}">
    <c:redirect url="/paymentsystem/return"/>
</c:if>

<c:if test="${sessionScope.administrator!=null}">
    <c:redirect url="/admin/return"/>
</c:if>

<div id="indexLeftColumn">

    <fmt:message key="welcome"/>

    <form method="POST" action="/paymentsystem/redirect">
        <button class="reg_button"><fmt:message key="register"/></button>
    </form>

</div>

<div id="indexRightColumn">
    <form method="post" action="/paymentsystem/login">
        <table width="70%" border="0" cellspacing="0" cellpadding="10" class="login_form">
            ${error}
            <tr>
                <td width="35%">e-mail</td>
                <td width="65%"><input type="text" name="email" value="${email}"></td>
            </tr>
            <tr>
                <td><fmt:message key="password"/></td>
                <td><input type="password" name="password"></td>
            </tr>
            <tr>
                <td>&nbsp;</td>
                <td>
                    <button class="button"><fmt:message key="login"/></button>
                </td>
            </tr>

        </table>
    </form>
</div>
