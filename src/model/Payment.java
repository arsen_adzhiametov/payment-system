package model;

import java.io.Serializable;
import java.util.Calendar;

public class Payment implements Serializable {
    private Integer id;
    private String action;
    private Double amount;
    private Calendar time = Calendar.getInstance();

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getAction() {
        return action;
    }

    public void setAction(String action) {
        this.action = action;
    }

    public Double getAmount() {
        return amount;
    }

    public void setAmount(Double amount) {
        this.amount = amount;
    }

    public Calendar getTime() {
        return time;
    }

    public void setTime(Calendar time) {
        this.time = time;
    }

    @Override
    public String toString() {
        return "Payment #"+getId()+" action";
    }
}
