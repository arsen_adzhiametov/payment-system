package dao.daoInterfaces;

import model.Payment;

import java.util.List;

public interface PaymentDAO {

    Payment findPayment(Integer id);

    List<Payment> findAll(Integer accountId);

    boolean addPayment(Payment payment, Integer accountId);

    List<Payment> findFew(Integer accountId, int lastId);

}
