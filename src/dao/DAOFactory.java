package dao;

import dao.daoInterfaces.*;

import java.sql.Connection;
import java.sql.SQLException;

public abstract class DAOFactory {

    public enum ConnTypes {
        DriverManagerJDBC, DataSourceJDBC
    }

    public static DAOFactory getDaoFactory(ConnTypes connType) {
        switch (connType) {
            case DriverManagerJDBC:
                return new DriverManagerDAOJDBCFactory();
            case DataSourceJDBC:
                return new DataSourceDAOJDBCFactory();
            default:
                return null;
        }
    }

    ;

    public abstract Connection getConnection() throws SQLException;

    public abstract ClientDAO getClientDAO();

    public abstract AccountDAO getAccountDAO();

    public abstract CardDAO getCardDAO();

    public abstract PaymentDAO getPaymentDAO();

    public abstract AdministratorDAO getAdministratorDAO();
}
