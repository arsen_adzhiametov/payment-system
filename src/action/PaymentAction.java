package action;

import dao.DAOFactory;
import dao.DataSourceDAOFactory;
import dao.daoInterfaces.PaymentDAO;
import model.Account;
import model.Payment;
import org.apache.log4j.Logger;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

public class PaymentAction implements Action {

    private static final Logger LOG = Logger.getLogger(PaymentAction.class);

    private static final double MONEY_ON_CREATE = 20;
    private static final String ACTION_ON_CREATE = "Создание счета";
    private static final String ADDED_MONEY = "Положено на счет";
    private static final String EXPEND_MONEY = "Снято со счета";

    @Override
    public String execute(HttpServletRequest request, HttpServletResponse response) throws Exception {
        DAOFactory daoFactory = DataSourceDAOFactory.getDAOFactory();
        PaymentDAO paymentDAO = daoFactory.getPaymentDAO();

        Account account = (Account) request.getSession().getAttribute("account");

        Payment payment = new Payment();
        if (request.getPathInfo().equals("/pay")) {
            payment.setAction(EXPEND_MONEY);
            payment.setAmount(Double.parseDouble(request.getParameter("amount")));
            LOG.info("Trying to expend money");
        } else if (request.getPathInfo().equals("/recharge")) {
            payment.setAction(ADDED_MONEY);
            payment.setAmount(Double.parseDouble(request.getParameter("amount")));
            LOG.info("Trying to add money");
        } else {
            payment.setAction(ACTION_ON_CREATE);
            payment.setAmount(MONEY_ON_CREATE);
            LOG.info("Trying first payment");
        }

        boolean added = paymentDAO.addPayment(payment, account.getId());

        if (added) {
            LOG.info("Payment added");
            account.getPayments().add(payment);
            return "client_profile";
        } else {
            LOG.error("Payment not added");
            request.setAttribute("error", "Не удалось зажурналировать платеж. Простите.");
            return "failed";
        }
    }

}
